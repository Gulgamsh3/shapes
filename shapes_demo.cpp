#include "shapes.h"
#include <iostream>
#include <vector>

using namespace std;

int main()
{
  vector<Shape*> shapes;

  shapes.push_back(new Circle(10));
  shapes.push_back(new Square(5));
  for (unsigned i = 0; i < shapes.size(); ++i)
    std::cout << "area of shape " << i << " is " << shapes[i]->area()<<" ";
  for (Shape * s : shapes)
    delete s;
  shapes.clear();
}
